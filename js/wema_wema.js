var domIsReady = (function(domIsReady) {
   var isBrowserIeOrNot = function() {
      return (!document.attachEvent || typeof document.attachEvent === "undefined" ? 'not-ie' : 'ie');
   }

   domIsReady = function(callback) {
      if(callback && typeof callback === 'function'){
         if(isBrowserIeOrNot() !== 'ie') {
            document.addEventListener("DOMContentLoaded", function() {
               return callback();
            });
         } else {
            document.attachEvent("onreadystatechange", function() {
               if(document.readyState === "complete") {
                  return callback();
               }
            });
         }
      } else {
         console.error('The callback is not a function!');
      }
   }

   return domIsReady;
})(domIsReady || {});

(function(document, window, domIsReady, undefined) {
    domIsReady(function() {
        document.querySelectorAll('input[type="text"]').forEach(function(e) {
            e.addEventListener('keyup', function() {
                refreshText();
            });
            e.addEventListener('paste', function() {
                refreshText();
            });
        });
        document.querySelectorAll('input[type="radio"]').forEach(function(e) {
            e.addEventListener('click', function() {
                refreshText();
            });
        });
        document.querySelector('#download').addEventListener('click', function() {
            this.download = 'WeMake.png';
            this.href     = document.getElementById('canvas').toDataURL().replace("image/png", "image/octet-stream");
        });

        if (window.location.hash) {
            document.querySelector('#porn').value = decodeURIComponent(window.location.hash.substr(1));
        }

        refreshText();
    });
})(document, window, domIsReady);

function refreshText(useValue) {
    var w    = document.getElementById('wemake');
    var p    = document.getElementById('porn');
    var c    = document.getElementById('canvas');
    var ctx  = c.getContext('2d');
        ctx.clearRect(0, 0, c.width, c.height);

    // update URL fragment
    window.location.hash = "#" + p.value;

    // background color
    var radius = 20;
    var corners = document.getElementsByName('corners');
    for (var i = 0; i < corners.length; i++) {
        if (corners[i].checked) {
            radius = parseInt(corners[i].value);
        }
    }
    ctx.fillStyle = 'rgba(252, 210, 5, 1)';
    roundRect(ctx, 0, 0, c.width, c.height, radius, true, false);

    // rounded rectangle
    ctx.lineWidth   = 20;
    ctx.strokeStyle = 'rgb(0, 0, 0)';
    roundRect(ctx, 25, 25, 750, 350, 20, false);

    ctx.textAlign="center";
    // Write WE MAKE
    ctx.font      = 'bold 150px sans-serif';
    ctx.fillStyle = 'rgba(0, 0, 0, 1)';
    ctx.fillText(w.value, 400, 160, 725);

    // Write new value
    ctx.font      = 'bold 220px sans-serif';
    ctx.fillStyle = 'rgba(0, 0, 0, 1)';
    ctx.fillText(p.value, 400, 350, 725);
}

function roundRect(ctx, x, y, width, height, radius, fill, stroke) {
    if (typeof stroke == 'undefined') {
        stroke = true;
    }
    if (typeof radius === 'undefined') {
        radius = 5;
    }
    if (typeof radius === 'number') {
        radius = {tl: radius, tr: radius, br: radius, bl: radius};
    } else {
        var defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};
        for (var side in defaultRadius) {
            radius[side] = radius[side] || defaultRadius[side];
        }
    }
    ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    ctx.closePath();
    if (fill) {
        ctx.fill();
    }
    if (stroke) {
        ctx.stroke();
    }

}

